<?php

namespace Application\Service;

interface VideoServiceInterface
{

    /**
     * Find videos by a search term
     *
     * @param  string  $text Search term
     * @param  integer $page Page to return
     * @return VideoCollection
     */
    public function search($text, $page = 1);

    /**
     * Get video details by its id
     *
     * @param  string $id Video id
     * @return Video
     */
    public function findById($id);
}
