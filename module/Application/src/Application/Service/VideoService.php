<?php

namespace Application\Service;

use Exception;
use Dailymotion;
use Application\Model\Video;

class VideoService implements VideoServiceInterface
{
    /**
     * @inheritdoc
     */
    public function search($text, $page = 1)
    {
        $videos = [
            'total' => 0,
            'pages' => 0,
            'page' => $page,
            'data' => [],
        ];

        $result = $this->request('/videos', [
            'fields' => ['id', 'title', 'channel', 'owner'],
            'search' => $text,
            'limit' => 10,
            'page' => $page,
        ]);

        if (!empty($result['list'])) {
            foreach ($result['list'] as $row) {
                $videos['data'][] = $this->findById($row['id']);
            }
        }

        $videos['total'] = !empty($result['total']) ? $result['total'] : 0;
        $videos['pages'] = !empty($result['total']) ? round($result['total'] / $result['limit']) : 0;
        $videos['page'] = !empty($result['page']) ? $result['page'] : 0;

        return $videos;
    }

    /**
     * @inheritdoc
     */
    public function findById($id)
    {
        $video = $this->request('/video/' . $id, [
            'fields' => ['id', 'title', 'channel', 'owner'],
        ]);

        if (!empty($video)) {
            $model = new Video();

            $model->setId($video['id']);
            $model->setTitle($video['title']);
            $model->setChannel($video['channel']);
            $model->setOwner($video['owner']);

            return $model;
        }
    }

    /**
     * Make a request to the Dailmotion API
     * @param  string $url API endpoint
     * @param  array $params Any parameters to be passed
     * @return array
     */
    private function request($url, $params)
    {
        static $dailymotion = null;
        if ($dailymotion === null) {
            $dailymotion = new Dailymotion();
        }

        try {
            return $dailymotion->get($url, array_filter($params));
        } catch (Exception $e) {
            // For now just discard the error
        }
    }
}
