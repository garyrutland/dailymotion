<?php

namespace Application\Model;

interface VideoInterface
{
    /**
     * Will return the ID of the video
     * @return int
     */
    public function getId();
}
