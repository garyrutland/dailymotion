<?php

namespace Application\Model;

class Video implements VideoInterface
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $channel;

    /**
     * @var string
     */
    protected $owner;

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the video ID
     * @param int $id The video ID
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Will return the title of the video
     * @return string The video title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set the video title
     * @param string $title The video title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Will return the video channel
     * @return string The video channel
     */
    public function getChannel()
    {
        return $this->channel;
    }

    /**
     * Set the video channel
     * @param string $channel The video channel
     */
    public function setChannel($channel)
    {
        $this->channel = $channel;
    }

    /**
     * Will return the video owner
     * @return string The video owner
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * Set the video owner
     * @param string $owner The video owner
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;
    }
}
