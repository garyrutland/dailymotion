<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Application\Service\VideoServiceInterface;

class VideoController extends AbstractActionController
{
    /**
     * @var \Application\Service\VideoServiceInterface
     */
    protected $videoService;

    /**
     * @inheritdoc
     */
    public function __construct(VideoServiceInterface $videoService)
    {
        $this->videoService = $videoService;
    }

    /**
     * The index action
     * @return ViewModel The view model
     */
    public function indexAction()
    {
        $id = $this->params()->fromRoute('id');
        $video = $this->videoService->findById($id);
        $referer = $this->getRequest()->getHeader('referer')->getUri();

        if ($video === null) {
            return $this->notFoundAction();
        }

        return new ViewModel([
            'referer' => $referer,
            'video' => $video,
        ]);
    }
}
