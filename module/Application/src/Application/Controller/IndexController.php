<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Application\Form\SearchForm;

class IndexController extends AbstractActionController
{
    /**
     * The index action
     * @return ViewModel The view model
     */
    public function indexAction()
    {
        return new ViewModel();
    }
}
