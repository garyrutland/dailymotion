<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Application\Form\VideoForm;
use Application\Service\VideoServiceInterface;

class SearchController extends AbstractActionController
{
    /**
     * @var \Application\Service\VideoServiceInterface
     */
    protected $videoService;

    /**
     * @inheritdoc
     */
    public function __construct(VideoServiceInterface $videoService)
    {
        $this->videoService = $videoService;
    }

    /**
     * The index action
     * @return ViewModel The view model
     */
    public function indexAction()
    {
        $form = new VideoForm();
        $request = $this->getRequest();
        $page = $this->params()->fromRoute('page', 1);
        $keywords = null;

        $form->setData($request->getQuery());
        if ($form->isValid()) {
            // For some reason this wasn't working
            // $keywords = $form->getValue('keywords');

            // For now, defaulted to getting the data array from the form
            $data = $form->getData();
            $keywords = $data['keywords'];
        }

        return new ViewModel([
            'form' => $form,
            'keywords' => $keywords,
            'page' => $page,
            'videos' => $this->videoService->search($keywords, $page),
        ]);
    }
}
