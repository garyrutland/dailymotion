<?php

namespace Application\Form;

use Zend\Form\Form;

class VideoForm extends Form
{
    /**
     * @inheritdoc
     */
    public function __construct($name = null)
    {
        parent::__construct('video');

        $this->setAttribute('method', 'GET');

        $this->add([
            'name' => 'keywords',
            'type' => 'text',
            'options' => [
                'label' => 'Keywords',
            ],
            'attributes' => [
                'class' => 'form-control',
             ],
        ]);

        $this->add([
             'name' => 'submit',
             'type' => 'button',
             'options' => [
                 'label' => 'Go',
             ],
             'attributes' => [
                'type'  => 'submit',
                'class' => 'btn btn-success',
             ],
         ]);
    }
}
